FROM ubuntu:noble

LABEL maintainer="Apex.AI <opensource@apex.ai>"

# Fix locale
RUN apt-get update && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt-get install -y \
        sudo \
        locales \
        tzdata \
        software-properties-common
RUN locale-gen en_US.UTF-8; dpkg-reconfigure -f noninteractive locales
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

RUN add-apt-repository universe

WORKDIR /ros2_jazzy

# Add the ROS apt repository, then install away
# Adapted from https://docs.ros.org/en/jazzy/Installation/Alternatives/Ubuntu-Development-Setup.html
RUN apt-get install -y \
        curl \
        gnupg2 && \
    curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu noble main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null && \
    apt-get update && \
    apt-get update && apt-get install -y \
        python3-flake8-blind-except \
        python3-flake8-class-newline \
        python3-flake8-deprecated \
        python3-mypy \
        python3-pip \
        python3-pytest \
        python3-pytest-cov \
        python3-pytest-mock \
        python3-pytest-repeat \
        python3-pytest-rerunfailures \
        python3-pytest-runner \
        python3-pytest-timeout \
        ros-dev-tools && \
    mkdir -p /ros2_jazzy/src && \
    wget https://raw.githubusercontent.com/ros2/ros2/jazzy/ros2.repos && \
    vcs import src < ros2.repos

RUN sudo rosdep init

# Note. rosdep update and rosdep install shall not be running in sudo mode
RUN rosdep update && \
    rosdep install --from-paths src --ignore-src --rosdistro jazzy -y --skip-keys "fastcdr rti-connext-dds-6.0.1 urdfdom_headers" && \
    rm -rf /ros2_jazzy

# Install ros-github-scripts
RUN git clone https://github.com/ros-tooling/ros-github-scripts
# Workaround to use PyGithub version from pip
RUN apt-get purge -y python3-github
RUN pip3 install --break-system-packages ./ros-github-scripts && \
    rm -rf /ros-github-scripts

# Install CCACHE
RUN apt install -y ccache && \
    rm -rf /var/lib/apt/lists/*
