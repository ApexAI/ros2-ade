# ros2-ade

Provides an ADE base image with ROS 2 dependencies pre-installed, for building ROS 2 according to the [official instructions](https://docs.ros.org/en/rolling/Installation/Linux-Development-Setup.html).

Additionally, it provides an ADE volume with ROS 2 binaries.

## Supported ROS 2 distros

* Rolling
* Jazzy
* Iron
* Humble
* Galactic
* Foxy (EOL)
* Eloquent (EOL)
* Dashing (EOL)

## Prerequisites

You need to install [ADE](https://ade-cli.readthedocs.io/en/latest/index.html) and create
your [ADE home](https://ade-cli.readthedocs.io/en/latest/usage.html), which will be assumed to be
`~/ade-home` in the following.

First, download the desired `.aderc` corresponding to the ROS 2 distribution you want to use. In the
rest of the document, the `rolling` ROS 2 distribution is used as an example.

```shell
mkdir -p ~/ade-home/ros2_rolling
cd ~/ade-home/ros2_rolling
wget https://gitlab.com/ApexAI/ros2-ade/-/raw/master/.aderc-rolling -O .aderc
```

## Usage

If you only want to use ROS 2, the easiest way is to use the [pre-installed ROS
binaries](#using-the-pre-installed-ros-2). If you want to contribute to the development of ROS 2 or
you just need to build ROS 2 with specific flags (e.g. to instrument the code), it is also possible
to [build ROS 2 from source](#building-ros-2-from-source).

Optionally, you may want to adjust the `.aderc` file before getting started. If you do not require
preinstalled ROS 2 binaries, you can remove the binary ADE volume from the list of images, for
example. Additionally, you might want to add a volume with development tools, such as an IDE.

In the rest of the document, the `rolling` ROS 2 distribution is used as an example.

### Using the pre-installed ROS 2

`ros2-ade` provides volumes that contain ROS 2 binaries. To run these, source ROS 2 and start
using it as usual. Replace "rolling" with the distribution you want to use.

```bash
$ cd ~/ade-home/ros2_rolling
$ ade start --update --enter
ade$ source /opt/ros/rolling/setup.bash
ade$ ros2 run <package> <executable>
```

### Building ROS 2 from source

#### One-time setup

First, create the directory inside `~/ade-home` that will contain the ROS 2 source code. The source
checkout will then persist between runs of `ade`. Replace "rolling" with the distribution you want
to use.

```bash
mkdir -p ~/ade-home/ros2_rolling/src
```

Next, check out the ROS 2 sources. This is easiest to do in the ADE container because it has the
`vcs` tool already installed. The `ade$` prompt shows which lines are run inside it.

```shell
$ ade start --update --enter
# or if the container was already running
$ ade enter

ade$ cd ros2_rolling
# Note that "master" needs to be replaced with the distro here if you're not building Rolling
ade$ wget https://raw.githubusercontent.com/ros2/ros2/master/ros2.repos
ade$ vcs import src < ros2.repos
```

#### Building

Now you're ready to build ROS 2 – all dependencies are already installed. Make sure you're in the
ADE container, and in the correct directory, then run

```shell
ade$ colcon build --merge-install
```

## Optional: Building the ADE base image manually

This should not normally be necessary, if you want to extend this image there are better options, like ADE volumes.

However, if you want to build it yourself, here is how to do that (replace the ROS 2 distribution
with the distribution you want to use):

```shell
cd ~/ade-home
git clone git@gitlab.com:ApexAI/ros2-ade.git
cd ~/ade-home/ros2-ade/rolling
docker build -t ros2-ade-base:rolling .
cd ~/ade-home/ros2-ade/ade
docker build -t ros2-ade:rolling --build-arg ROS_DISTRO=rolling .
```

Note that the `.aderc` files need to be changed to use the local images instead of the container registry.
