FROM ubuntu:focal

LABEL maintainer="Apex.AI <opensource@apex.ai>"

# Fix locale
RUN apt update && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime && \
    apt install -y \
        sudo \
        locales \
        tzdata
RUN locale-gen en_US.UTF-8; dpkg-reconfigure -f noninteractive locales
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8

WORKDIR /ros2_galactic

# Add the ROS apt repository, then install away
# Adapted from https://docs.ros.org/en/galactic/Installation/Ubuntu-Development-Setup.html
RUN sudo apt install -y \
        curl \
        gnupg2 && \
    sh -c 'curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg' && \
    sh -c 'echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu focal main" | tee /etc/apt/sources.list.d/ros2.list > /dev/null' && \
    apt update && \
    apt install -y  \
        bash-completion \
        build-essential \
        cmake \
        git \
        libbullet-dev \
        python3-colcon-common-extensions \
        python3-flake8 \
        python3-pip \
        python3-pytest-cov \
        python3-rosdep \
        python3-setuptools \
        python3-vcstool \
        acl \
        libacl1-dev \
        vim \
        wget \
        qtbase5-dev \
        libqt5svg5-dev \
        libqt5websockets5-dev \
        libqt5opengl5-dev \
        libqt5x11extras5-dev && \
    apt install --no-install-recommends -y \
        libasio-dev \
        libtinyxml2-dev \
        libcunit1-dev && \
    python3 -m pip install -U \
        argcomplete \
        colcon-mixin \
        flake8-blind-except \
        flake8-builtins \
        flake8-class-newline \
        flake8-comprehensions \
        flake8-deprecated \
        flake8-docstrings \
        flake8-import-order \
        flake8-quotes \
        pytest-repeat \
        pytest-rerunfailures \
        pytest && \
    mkdir -p /ros2_galactic/src && \
    wget https://raw.githubusercontent.com/ros2/ros2/galactic/ros2.repos && \
    vcs import src < ros2.repos

RUN sudo rosdep init

# Note. rosdep update and rosdep install shall not be running in sudo mode
RUN rosdep update && \
    rosdep install --from-paths src --ignore-src --rosdistro galactic -y --skip-keys "console_bridge fastcdr fastrtps rti-connext-dds-5.3.1 urdfdom_headers" && \
    rm -rf /ros2_galactic

# Install ros-github-scripts
RUN git clone https://github.com/ros-tooling/ros-github-scripts
RUN pip3 install ./ros-github-scripts && \
    rm -rf /ros-github-scripts

# Install CCACHE
RUN apt install -y ccache && \
    rm -rf /var/lib/apt/lists/*
